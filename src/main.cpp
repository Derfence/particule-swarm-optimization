#include <iostream>
#include <omp.h>
#include <time.h>
#include "PSO.hpp"

int main(int argc, char **argv)
{
    bool verbose = false;
    if (argc <= 3)
        return 1;
    srand(time(NULL));

    unsigned validSolutions = 0;
    unsigned nbTests = atoi(argv[3]);
#pragma omp parallel for
    for (unsigned testId = 0; testId < nbTests; testId++)
    {
        //Creation of an instance
        unsigned nbVertices = 1 + rand() % 100;
        if (verbose)
            cout << "nbVertices = " << nbVertices << endl;
        vector<vector<unsigned>> adjacencyMatrix = vector<vector<unsigned>>(nbVertices, vector<unsigned>(nbVertices));
        for (unsigned i = 1; i < nbVertices; i++)
        {
            for (unsigned j = 0; j < i; j++)
            {
                adjacencyMatrix[i][j] = rand() % 2;
                adjacencyMatrix[j][i] = adjacencyMatrix[i][j];
                if (verbose)
                    cout << adjacencyMatrix[i][j] << " ";
            }
            if (verbose)
                cout << endl;
        }

        //PSO
        Graph graph = Graph(adjacencyMatrix);
        Particule sol = PSO::Resolve(graph, atoi(argv[2]), atoi(argv[1]));
        if (verbose)
        {
            for_each(sol.begin(), sol.end(), [](unsigned &it) { cout << it << " "; });
            cout << endl;
        }

        //Verify
        bool valid = true;
        for (unsigned i = 0; i < nbVertices; i++)
        {
            if (sol[i] < 0.5 && valid)
            {
                unsigned j = -1;
                while (++j < nbVertices && (graph.adjacencyMatrix[i][j] ? sol[j] >= 0.5 : 1)) //If stopped before nbVertice : edge i-j exist & i = j = not colored
                    ;
                if (j != nbVertices)
                    valid = false;
            }
        }
        validSolutions += valid;
    }

    cout << validSolutions * 100.0 / nbTests << "% corrects" << endl;
    return 0;
}
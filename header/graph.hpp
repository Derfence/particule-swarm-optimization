#ifndef GRAPH_HPP
#define GRAPH_HPP

#include <vector>
#include <algorithm>

using namespace std;

class Graph
{
public:
    unsigned nbVertices;
    vector<vector<unsigned>> adjacencyMatrix;

    Graph(unsigned nbVertices) : nbVertices(nbVertices), adjacencyMatrix(vector<vector<unsigned>>(nbVertices, vector<unsigned>(nbVertices))) {}
    Graph(vector<vector<unsigned>> adjacencyMatrix) : nbVertices(adjacencyMatrix.size()), adjacencyMatrix(adjacencyMatrix) {}
    ~Graph() {}
};

#endif
#ifndef PSO_HPP
#define PSO_HPP

#include <cstdlib>
#include <numeric>
#include <cmath>
#include <tuple>
#include "graph.hpp"

using namespace std;

typedef vector<unsigned> Particule;

class PSO
{
public:
    static Particule Resolve(Graph &graph, unsigned iterMax, unsigned nbParticules)
    {
        //On considerera qu'un noeud de valeur < 0.5 vaut 0 cad n'étant pas colorié
        //et un noeud de valeur >= 0.5 vaut 1 cad étant colorié
        vector<Particule> particules = vector<Particule>(nbParticules, Particule(graph.nbVertices));
        vector<Particule> velocities = vector<Particule>(nbParticules, Particule(graph.nbVertices));
        vector<tuple<Particule, double>> localBest = vector<tuple<Particule, double>>(nbParticules, make_tuple(Particule(graph.nbVertices), numeric_limits<double>::max()));
        tuple<Particule, double> globalBest = make_tuple(Particule(graph.nbVertices), numeric_limits<double>::max());

        //Initial set
        for_each(particules.begin(), particules.end(),
                 [](Particule &p) { for_each(p.begin(), p.end(),
                                             [](unsigned &it) { it = rand() % 2; }); });
        //Error function
        auto error = [&graph](Particule &p) -> int {
            static double lambda = 1.2;
            unsigned sum = 0;
            for (unsigned i = 0; i < graph.nbVertices; i++)
            {
                double subSum = 0;
                if (p[i] < 0.5) // non colorié
                    for (unsigned j = 0; j < graph.nbVertices; j++)
                        subSum += graph.adjacencyMatrix[i][j] && p[j] < 0.5;
                sum += lambda * subSum - 1;
            }
            return sum;
        };

        //particule swarm optimization
        unsigned iter = 0;
        double c1 = 1.5, c2 = 2.6, c = c1 + c2, khi = 2.0 / sqrt(2 - c - sqrt(c * c - 4 * c));
        while (iter < iterMax)
        {
            iter++;
            //For each particules
            for (unsigned i = 0; i < nbParticules; i++)
            {
                //Update velocity & position
                for (unsigned node = 0; node < graph.nbVertices; node++)
                {
                    double local = c1 * rand() / RAND_MAX * (get<0>(localBest[i])[node] - particules[i][node]);
                    double global = c2 * rand() / RAND_MAX * (get<0>(globalBest)[node] - particules[i][node]);
                    velocities[i][node] = khi * (velocities[i][node] + local + global);

                    particules[i][node] += velocities[i][node];
                }

                //Update best
                int e = error(particules[i]);
                if (get<1>(localBest[i]) > e)
                {
                    for (unsigned node = 0; node < graph.nbVertices; node++)
                    {
                        get<0>(localBest[i])[node] = particules[i][node];
                    }
                    get<1>(localBest[i]) = e;

                    if (get<1>(globalBest) > e)
                    {
                        for (unsigned node = 0; node < graph.nbVertices; node++)
                        {
                            get<0>(globalBest)[node] = particules[i][node];
                        }
                        get<1>(globalBest) = e;
                    }
                }
            }
        }

        return get<0>(globalBest);
    }
};

#endif